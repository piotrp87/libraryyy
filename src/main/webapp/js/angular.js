var app = angular.module('app', []);

app.controller('postController', function($scope, $http, $location) {
	$scope.submitForm = function(){
		var url = $location.absUrl() + "postuser";

		var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
        }

		var data = {
            firstName: $scope.firstname,
            lastName: $scope.lastname,
            telephone: $scope.telephone
        };


		$http.post(url, data, config).then(function (response) {
			$scope.postResultMessage = "Sucessful!";
		}, function (response) {
			$scope.postResultMessage = "Fail!";
		});

		$scope.firstname = "";
		$scope.lastname = "";
		$scope.telephone = "";
	}
});

app.controller('postControllerBook', function($scope, $http, $location) {
	$scope.submitForm = function(){
		var url = $location.absUrl() + "postbook";

		var config = {
                headers : {
                    'Content-Type': 'application/json;charset=utf-8;'
                }
        }

		var data = {
			authors: $scope.authorsID,
            //  authors: $scope.authorsID,
            title: $scope.title,
			rental: $scope.rentalId,
			isBorrowed: $scope.borrowed,
			isDamaged: $scope.damaged,
        };


		$http.post(url, data, config).then(function (response) {
			$scope.postResultMessage = "Sucessful!";
		}, function (response) {
			$scope.postResultMessage = "Fail!";
		});

		$scope.authorsID= "";
		$scope.title= "";
		$scope.rentalId="",
		$scope.details= "";
		$scope.borrowed=  "false";
		$scope.damaged= "false";
	}
});



app.controller('getallusersController', function($scope, $http, $location) {

	$scope.showAllUsers = false;

	$scope.getAllUsers = function() {
		var url = $location.absUrl() + "findall";

		var config = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		// $http.get(url, config).then([Request processing failed; nested exception is java.lang.NullPointerException] with root cause(response) {
			$http.get(url, config).then(function(response) {
			if (response.data.status == "Done") {
				$scope.allusers = response.data;
				$scope.showAllUsers = true;

			} else {
				$scope.getResultMessage = "get All Users Data Error!";
			}

		}, function(response) {
			$scope.getResultMessage = "Fail!";
		});

	}
});

app.controller('getallbooksController', function($scope, $http, $location) {

	$scope.showAllBooks = false;

	$scope.getAllBooks = function() {
		var url = $location.absUrl() + "books";

		var config = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		$http.get(url, config).then(function(response) {

			// if (response.data.status == "Done") {
				$scope.allbooks = response.data;
				$scope.showAllBooks = true;

		// 	// } else {
		// 		$scope.getResultMessage = "get All Books Data Error!";
		// 	}

		// }, function(response) {
		// 	$scope.getResultMessage = "Fail!";
		// });
		})
	}
});

//user by id ponizej:
app.controller('getuserController', function($scope, $http, $location) {

	$scope.showUser = false;

	$scope.getUser = function() {
		var url = $location.absUrl() + "user/" + $scope.userId;

		var config = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		$http.get(url, config).then(function(response) {

			if (response.data.status == "Done") {
				$scope.user = response.data;
				$scope.showUser = true;

			} else {
				$scope.getResultMessage = "User Data Error!";
			}

		}, function(response) {
			$scope.getResultMessage = "Fail!";
		});

	}
});
//pobieranie ksiazki by ID:
app.controller('getbookByIdController', function($scope, $http, $location) {

	$scope.showBook = false;

	$scope.getBook = function() {
		var url = $location.absUrl() + "book/" + $scope.bookId;

		var config = {
			headers : {
				'Content-Type' : 'application/json;charset=utf-8;'
			}
		}

		$http.get(url, config).then(function(response) {

			// if (response.data.status == "Done") {
				$scope.book = response.data;
				$scope.showBook = true;

			// } else {
			// 	$scope.getResultMessage = "Book Data Error!";
			// }

		// }, function(response) {
		// 	$scope.getResultMessage = "Fail!";
		// });
	})

	}
});








app.controller('getusersbylastnameController', function($scope, $http, $location) {

	$scope.showUsersByLastName = false;

	$scope.getUsersByLastName = function() {
		var url = $location.absUrl() + "findbylastname";

		var config = {
			headers : {	'Content-Type' : 'application/json;charset=utf-8;' },

			params: { 'lastName' : $scope.userLastName }
		}

		$http.get(url, config).then(function(response) {

			if (response.data.status == "Done") {
				$scope.allusersbylastname = response.data;
				$scope.showUsersByLastName = true;

			} else {
				$scope.getResultMessage = "User Data Error!";
			}

		}, function(response) {
			$scope.getResultMessage = "Fail!";
		});

	}
});







//get by title book:
app.controller('getbooksbyTitleController', function($scope, $http, $location) {

	$scope.showBooksByTitle = false;

	$scope.getbooksbyTitle = function() {
		var url = $location.absUrl() + "findbytitle";

		var config = {
			headers : {	'Content-Type' : 'application/json;charset=utf-8;' },

			//  params: { 'title' : $scope.bookByTitle }
			params: { 'title' : $scope.bookByTitle }
		}

		$http.get(url, config).then(function(response) {

			// if (response.data.status == "Done") {
				$scope.allbooksbytitle = response.data;
				$scope.showBooksByTitle = true;

		// 	} else {
		// 		$scope.getResultMessage = "Book Data Error!";
		// 	}

		// }, function(response) {
		// 	$scope.getResultMessage = "Fail   @@!";
		// });
	})
	}
});