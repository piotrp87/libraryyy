<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<title>Spring Boot Example</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<script
	src="http://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script>
<script src="/js/angular.js"></script>
<link rel="stylesheet"
	href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" />
</head>
<body>
	<div class="container" ng-app="app">
		<h1>AngularJS - Spring JPA - PostgreSQL</h1>

		<div class="row">
			<div ng-controller="postController" class="col-md-3">
				<form name="userForm" ng-submit="submitForm()">
					<label>FirstName</label>
					<input type="text" name="firstname"	class="form-control" ng-model="firstname" />
					<label>LastName</label>
					<input type="text" name="lastname" class="form-control" ng-model="lastname" />
                    <label>Telephone</label>
                    <input type="text" name="telephone" class="form-control" ng-model="telephone" />
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
				<p>{{postResultMessage}}</p>
			</div>
		</div>
		<div class="row">
			<div ng-controller="getallusersController" class="col-md-3">
				<h3>All Customers</h3>

				<button ng-click="getAllUsers()">Get All Customers</button>

				<div ng-show="showAllUsers">
					<ul class="list-group">
						<li ng-repeat="user in allusers.data"><h4 class="list-group-item">
								<strong>Customer {{$index}}</strong><br />
								Id: {{user.id}}<br />
								First Name: {{user.firstName}}<br />
								Last Name: {{user.lastName}}
						</h4></li>
					</ul>
				</div>
				<p>{{getResultMessage}}</p>
			</div>

			<div ng-controller="getuserController" class="col-md-3">
				<h3>Customer by ID</h3>

				<input type="text" class="form-control" style="width: 100px;"
					ng-model="userId" /> <br />
				<button ng-click="getUser()">Get Customer ID</button>

				<div ng-show="showUser">
					Id: {{user.data.id}}<br />
					First Name: {{user.data.firstName}}<br />
					Last Name: {{user.data.lastName}}
				</div>
				<p>{{getResultMessage}}</p>
			</div>

			<div ng-controller="getusersbylastnameController" class="col-md-4">
				<h3>Customers by LastName</h3>

				<input type="text" class="form-control" style="width: 100px;" ng-model="userLastName" /><br />
				<button ng-click="getUsersByLastName()">Get Customers LastName</button>

				<div ng-show="showUsersByLastName">

					<ul class="list-group">
						<li ng-repeat="user in allusersbylastname.data"><h4	class="list-group-item">
								<strong>Customer {{$index}}</strong><br />
								Id: {{user.id}}<br />
								First Name: {{user.firstName}}<br />
								Last Name: {{user.lastName}}
						</h4></li>
					</ul>
				</div>
				<p>{{getResultMessage}}</p>
			</div>

		
		</div>








		<div class="row">
			<div ng-controller="postControllerBook" class="col-md-3">
				<form name="bookForm" ng-submit="submitForm()">
					<label>Author ID</label>
					<input type="text" name="authors" class="form-control" ng-model="authors" />
					<label>Title of Book</label>
					<input type="text" name="title" class="form-control" ng-model="title" />
                    <label>Details of Book</label>
                    <input type="text" name="details" class="form-control" ng-model="details" />
					<button type="submit" class="btn btn-primary">Submit Book</button>
				</form>
				<p>{{postResultMessage}}</p>
			</div>
		</div>




	
	<div class="row">
		<div ng-controller="getallbooksController" class="col-md-3">
			<h3>All Books</h3>
		
	
			<button ng-click="getAllBooks()">Get All Books</button>
			<div ng-show="showAllBooks">
				<ul class="list-group">
					<li ng-repeat="book in allbooks"><h4 class="list-group-item">
							<strong>Book {{$index}}</strong><br />
							Id: {{book.id}}<br />
							AuthorID: {{book.authorsID}}<br />
							Author name: {{book.Author}<br />
							Author name: {{book.AuthorID}<br />
							Author name: {{book.authors}<br />
							Author name: {{book.authorsID}<br />
							Title: {{book.title}}<br />
							Details: {{book.details}}<br />
							Rental ID: {{book.rental}}<br />
							Borrowed: {{book.isBorrowed}}<br />
							Damaged: {{book.isDamaged}}<br />
					</h4></li>
				</ul>
			</div>
			<p>{{getResultMessage}}</p>
		</div>

	</div>

	<div ng-controller="getbookByIdController" class="col-md-3">
		<h3>Book by ID</h3>

		<input type="text" class="form-control" style="width: 100px;"
			ng-model="bookId" /> <br />
		<button ng-click="getBook()">Get Book ID</button>

		<div ng-show="showBook">
			Id: {{book.id}}<br />
			Author: {{book.authors}}<br />
			Author name: {{book.authors.firstName}}<br />
			Title: {{book.title}}<br />
			Details: {{book.details}}<br />
			Borrowed?: {{book.isBorrowed}}<br />
			Damaged?: {{book.isDamaged}}<br />
		</div>
		<p>{{getResultMessage}}</p>
	</div>




	<div ng-controller="getbooksbyTitleController" class="col-md-4">
		<h3>Book by Title</h3>

		<input type="text" class="form-control" style="width: 100px;" ng-model="bookByTitle" /><br />
		<button ng-click="getbooksbyTitle()">Get Book by Title</button>

		<div ng-show="showBooksByTitle">

			<ul class="list-group">
				<li ng-repeat="book in allbooksbytitle"><h4	class="list-group-item">
						<strong>Book {{$index}}</strong><br />
						Id: {{book.id}}<br />
						Author name: {{book.authors.firstName}}<br />
						Author: {{book.authors}}<br />
						Title: {{book.title}}<br />
						Details: {{book.details}}<br />
						Borrowed?: {{book.isBorrowed}}<br />
						Damaged?: {{book.isDamaged}}<br />
						
				</h4></li>
			</ul>
		</div>
		<p>{{getResultMessage}}</p>
	</div>


		
	
	</div>
</body>
</html>