package com.library.lib.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table (name = "rentals")
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDate rented_at;
    private LocalDate returned_at;
    @OneToOne
    private Book book;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    private BigDecimal fine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getRented_at() {
        return rented_at;
    }

    public void setRented_at(LocalDate rented_at) {
        this.rented_at = rented_at;
    }

    public LocalDate getReturned_at() {
        return returned_at;
    }

    public void setReturned_at(LocalDate returned_at) {
        this.returned_at = returned_at;
    }

    public BigDecimal getFine() {
        return fine;
    }

    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    public Rental() {
    }

    public Rental( Book book, User user) {
        this.rented_at = LocalDate.now();
        this.returned_at = rented_at.plusDays(7);
        this.book = book;
        this.user = user;
        this.fine = BigDecimal.valueOf(0);
    }


}
