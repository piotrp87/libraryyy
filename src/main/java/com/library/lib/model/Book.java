package com.library.lib.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="books")
public class Book implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    private Author authors;
    //private Long authors;
    private String title;
    private String details;
    private boolean isBorrowed = false;
    private boolean isDamaged = false;
    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Rental rental;

    public Book() { }


    public Book( Author authors, String title, String details, boolean isBorrowed, boolean isDamaged) {
        this.authors = authors;
        this.title = title;
        this.details = details;
        this.isBorrowed = isBorrowed;
        this.isDamaged = isDamaged;
    }

    public Rental getRental() {
        return rental;
    }

    public void setRental(Rental rental) {
        this.rental = rental;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author getAuthors() {
        return authors;
    }

    public void setAuthors(Author authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public void setDamaged(boolean damaged) {
        isDamaged = damaged;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", authors=" + authors +
                ", title='" + title + '\'' +
                ", details='" + details + '\'' +
                ", isBorrowed=" + isBorrowed +
                ", isDamaged=" + isDamaged +
                '}';
    }
}
