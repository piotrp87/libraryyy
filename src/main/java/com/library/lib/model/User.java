package com.library.lib.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table( name = "users")
public class User
{

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue
    private Long id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "telephone")
    private Integer telephone;
    private String login;
    private String password;
    @Column(name = "is_admin")
    private boolean is_admin = false;
    @OneToMany(cascade = CascadeType.REFRESH)
    private Set<Rental> rentals;

    public User(){ }

    public User(String firstName, String lastName, Integer telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.telephone = telephone;
    }
    public Set<Rental> getRentals() {
        return rentals;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getTelephone() {
        return telephone;
    }

    public void setTelephone(Integer telephone) {
        this.telephone = telephone;
    }

    public void setRentals(Set<Rental> rentals) {
        this.rentals = rentals;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public boolean getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(boolean is_admin) {
        this.is_admin = is_admin;
    }


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", telephone=" + telephone +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", is_admin=" + is_admin +
                ", rentals=" + rentals +
                '}';
    }
}
