package com.library.lib.service;

import com.library.lib.mapper.BookMapper;
import com.library.lib.model.Author;
import com.library.lib.model.Book;
import com.library.lib.repo.AuthorRepository;
import com.library.lib.repo.BookRepository;
import com.library.lib.repo.RentalRepository;
import com.library.lib.web.BookDTO;
import com.library.lib.web.BookResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    private final BookRepository bookRepository;
    private final BookMapper bookMapper;
    private final AuthorRepository authorRepository;
    private final RentalRepository rentalRepository;

    public BookService(BookRepository bookRepository, BookMapper bookMapper, AuthorRepository authorRepository, RentalRepository rentalRepository) {
        this.bookRepository = bookRepository;
        this.bookMapper = bookMapper;
        this.authorRepository = authorRepository;
        this.rentalRepository = rentalRepository;
    }

    public List<Book> displayAllAuthors() {
        return bookRepository.findAll();
    }
    public Book findBook(Long id) {
        return bookRepository.findById(id);
    }

    public void add(BookDTO bookDto) {
        Book book = bookMapper.createNew(bookDto);
        Author authors = book.getAuthors();
        authors.getBooks().add(book);
    }

        public void addBook(BookDTO bookDto) {
            Book book = bookRepository.save(bookMapper.createNew(bookDto));
            Author author = book.getAuthors();
        }

//        bookRepository.save(book); to tez chyba ok
//        if(book.getRental() != null) {
//            rentalRepository.save(book.getRental());
//        }
//        authorRepository.save(author);
//    }


    public List<BookResponse> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(bookMapper::map)
                .collect(Collectors.toList());
    }
    public BookResponse getBook(Long id) {
//    public Book getBook(Long id) {
        Book book = bookRepository.findById(id);
//        Optional<Book> book = Optional.ofNullable(bookRepository.findById(id));
        return bookMapper.map(book);
//        return book.get();
    }

    public BookResponse getByTitle(String title){
//    public void getByTitle(String title){
        Book book = bookRepository.findByTitle(title);
        return bookMapper.map(book);
    }

    public void deleteBook(Long id) {
        Book book = bookRepository.findById(id);
        book.getRental().getUser().getRentals().remove(book.getRental());
        book.getRental().setUser(null);
        book.getRental().setBook(null);
        book.setRental(null);
        bookRepository.delete(book);
    }
}
