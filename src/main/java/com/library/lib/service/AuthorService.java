package com.library.lib.service;


import com.library.lib.mapper.AuthorMapper;
import com.library.lib.model.Author;
import com.library.lib.repo.AuthorRepository;
import com.library.lib.web.AuthorDTO;
import com.library.lib.web.AuthorResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;
    private final AuthorMapper authorMapper;

    public AuthorService(AuthorRepository authorRepository, AuthorMapper authorMapper) {
        this.authorRepository = authorRepository;
        this.authorMapper = authorMapper;
    }
    public List<AuthorResponse> getAllAuthors() {
        return authorRepository.findAll().stream()
                .map(authorMapper::map)
                .collect(Collectors.toList());
    }

    public void add(AuthorDTO createAuthorDto) {
        Author author = authorMapper.createNew(createAuthorDto);
        authorRepository.save(author);
    }
    public Author findAuthor(Long id) {
        return authorRepository.findById(id);
    }

}
