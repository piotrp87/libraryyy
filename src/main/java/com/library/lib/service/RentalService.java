package com.library.lib.service;

import com.library.lib.mapper.RentalMapper;
import com.library.lib.model.Book;
import com.library.lib.model.Rental;
import com.library.lib.model.User;
import com.library.lib.repo.BookRepository;
import com.library.lib.repo.RentalRepository;
import com.library.lib.repo.UsersRepository;
import com.library.lib.web.RentalDTO;
import com.library.lib.web.RentalResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RentalService {
    private final RentalRepository rentalRepository;
    private final RentalMapper rentalMapper;
    private final BookRepository bookRepository;
    private final UsersRepository usersRepository;

    public RentalService(RentalRepository rentalRepository, RentalMapper rentalMapper, BookRepository bookRepository, UsersRepository usersRepository) {
        this.rentalRepository = rentalRepository;
        this.rentalMapper = rentalMapper;
        this.bookRepository = bookRepository;
        this.usersRepository = usersRepository;
    }
    public RentalResponse getOne(Long id) {
        Rental rental = rentalRepository.findById(id);
        return rentalMapper.map(rental);
    }
    public List<RentalResponse> getAllRentals() {
        return rentalRepository.findAll().stream()
                .map(rentalMapper::map)
                .collect(Collectors.toList());
    }
    public void add(RentalDTO rentalDto) {
        Rental rental = rentalMapper.createNew(rentalDto);
        rentalRepository.save(rental);
        Book book = bookRepository.findById(rentalDto.getBookId());
        book.setRental(rental);
        User user = usersRepository.findById(rentalDto.getUserId());
        user.getRentals().add(rental);
        usersRepository.save(user);
        bookRepository.save(book);
    }
    public Rental findRental(Long rentalId) {
        return rentalRepository.findById(rentalId);
    }

    public void createRentals() {
        Rental rental = new Rental();
        Book book = bookRepository.findById(1l);
        rental.setBook(book);
        rental.setUser(usersRepository.findById(1L));
        rental.setRented_at(LocalDate.of(2019, 1, 1));
        rentalRepository.save(rental);
        book.setRental(rental);
        bookRepository.save(book);
    }


}
