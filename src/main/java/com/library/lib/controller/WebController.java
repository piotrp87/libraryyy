package com.library.lib.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@CrossOrigin("*")
@Controller
public class WebController {


    @RequestMapping("/")
    ModelAndView home(ModelAndView modelAndView) {

        modelAndView.setViewName("home");

        return modelAndView;
    }
}
