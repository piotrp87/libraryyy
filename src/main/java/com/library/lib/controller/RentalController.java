package com.library.lib.controller;


import com.library.lib.service.RentalService;
import com.library.lib.web.RentalDTO;
import com.library.lib.web.RentalResponse;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rental")
public class RentalController {


    private final RentalService rentalService;

    public RentalController(RentalService rentalService) {
        this.rentalService = rentalService;
    }

    @GetMapping("/id")
    public RentalResponse getOne(@PathVariable("id") Long id) {
        return rentalService.getOne(id);
    }

    @GetMapping
    public List<RentalResponse> getAll() {
        return rentalService.getAllRentals();
    }

    @PostMapping
    public void addRental(@RequestBody RentalDTO rentalDto) {
        rentalService.add(rentalDto);
    }
}
