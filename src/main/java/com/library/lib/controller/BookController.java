package com.library.lib.controller;


import com.library.lib.message.Response;
import com.library.lib.model.Book;
import com.library.lib.repo.BookRepository;
//import com.library.lib.controller.BookService;
import com.library.lib.service.BookService;
import com.library.lib.web.BookDTO;
import com.library.lib.web.BookResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("*")
@RestController
public class BookController {

    final private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

//    @Autowired
//    BookRepository repository;

    @RequestMapping(value = "/postbook", method = RequestMethod.POST)
    public void postBook(@RequestBody BookDTO bookDTO) {
    bookService.add(bookDTO); //lub samo .add
//        repository.save(new Book(book.getAuthorsId(), book.getTitle(),book. ,book.isBorrowed(),book.isDamaged()) );
    }

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    //public List<BookResponse> findAllBook(@RequestBody BookDTO book) {
    public List<BookResponse> findAllBook() {
        return bookService.getAllBooks();
//        Iterable<Book> books = repository.findAll();
//        return new Response("Done", books);
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.GET)
    public BookResponse findBookById(@PathVariable("id") long id) {

        //Book book = repository.findOne(id);

        return bookService.getBook(id);

    }
    @RequestMapping(value = "/findbytitle",method = RequestMethod.GET)
    public BookResponse findByTitle(@RequestParam("title") String title) {
//    public Response findByTitle(@RequestParam("title") String title) {

        return bookService.getByTitle(title);
//        List<Book> book = BookRepository.findByTitle(title);
//
//        return new Response("Done", book);
    }

//    @RequestMapping("/findbyauthor")
//    public Response findByAuthor(@RequestParam("author") String title) {
//
//        List<Book> book = repository.findByAuthor(title);
//
//        return new Response("Done", book);
//    }


}
