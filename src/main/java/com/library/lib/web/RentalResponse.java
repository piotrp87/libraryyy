package com.library.lib.web;

import java.math.BigDecimal;
import java.time.LocalDate;

public class RentalResponse {

    private Long id;
    private Long bookId;
    private Long userId;
    private LocalDate rented_at;
    private LocalDate returned_at;
    private BigDecimal fine;
    private String title;
    private String firstName;
    private String lastName;


    public RentalResponse(Long id, LocalDate rented_at) {
        this.id = id;
        this.rented_at = rented_at;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBookId() {
        return bookId;
    }

    public void setBookId(Long bookId) {
        this.bookId = bookId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public LocalDate getRented_at() {
        return rented_at;
    }

    public void setRented_at(LocalDate rented_at) {
        this.rented_at = rented_at;
    }

    public LocalDate getReturned_at() {
        return returned_at;
    }

    public void setReturned_at(LocalDate returned_at) {
        this.returned_at = returned_at;
    }

    public BigDecimal getFine() {
        return fine;
    }

    public void setFine(BigDecimal fine) {
        this.fine = fine;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
