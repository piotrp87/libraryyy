package com.library.lib.web;

import java.util.List;

public class AuthorDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private List<Long> booksId;

    public List<Long> getBooksId() {
        return booksId;
    }

    public void setBooksId(List<Long> booksId) {
        this.booksId = booksId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AuthorDTO() {
    }

    public AuthorDTO(Long id, String firstName, String lastName, List<Long> booksId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.booksId = booksId;
    }
}
