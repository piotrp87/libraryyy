package com.library.lib.web;

import java.util.HashSet;
import java.util.Set;

public class AuthorResponse {


        private Long id;
        private String firstName;
        private String lastName;
        private Set<Long> booksId = new HashSet<>();

        public AuthorResponse(Long id, String firstName, String lastName) {
            this.id = id;
            this.firstName = firstName;
            this.lastName = lastName;
        }

        public Set<Long> getBooksId() {
            return booksId;
        }

        public void setBooksId(Set<Long> booksId) {
            this.booksId = booksId;
        }

        public Long getId() {
            return id;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }
}

