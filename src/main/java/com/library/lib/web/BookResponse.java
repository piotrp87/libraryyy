package com.library.lib.web;

public class BookResponse {

    private Long id;
    private String title;
    private String details;
    private boolean isBorrowed;
    private boolean isDamaged;
    private Long authorsId;
    private Long rentalId;

    public BookResponse(Long id, String title, String details, boolean isBorrowed, boolean isDamaged,Long authorsId) {
        this.id = id;
        this.title = title;
        this.isBorrowed = isBorrowed;
        this.isDamaged = isDamaged;
        this.details = details;
        this.authorsId = authorsId;
    }

    public BookResponse(){}

    public void setAuthorsId(Long authorsId) {
        this.authorsId = authorsId;
    }

    public Long getRentalId() {
        return rentalId;
    }

    public void setRentalId(Long rentalId) {
        this.rentalId = rentalId;
    }

    public Long getAuthorsId() {
        return authorsId;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    public boolean isDamaged() {
        return isDamaged;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
