package com.library.lib.mapper;

import com.library.lib.model.Author;
import com.library.lib.model.Book;
import com.library.lib.repo.BookRepository;
import com.library.lib.web.AuthorDTO;
import com.library.lib.web.AuthorResponse;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AuthorMapper {


    private final BookRepository bookRepository;

    public AuthorMapper(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    public Author createNew(AuthorDTO authorDto) {
        Author author = new Author();
        author.setFirstName(authorDto.getFirstName());
        author.setLastName(authorDto.getLastName());
        Set<Book> books = new HashSet<>();
        authorDto.getBooksId().forEach(id -> books.add(bookRepository.findById(id)));
        author.setBooks(books);
        return author;
    }

    public AuthorResponse map(Author authorEntity) {
        AuthorResponse authorResponse = new AuthorResponse(authorEntity.getId(), authorEntity.getFirstName(), authorEntity.getLastName());

        if(authorEntity.getBooks() != null) {
            for(Book book : authorEntity.getBooks()) {
                authorResponse.getBooksId().add(book.getId());
            }
        }

        return authorResponse;
    }
}
