package com.library.lib.mapper;

import com.library.lib.model.Rental;
import com.library.lib.repo.BookRepository;
import com.library.lib.repo.UsersRepository;
import com.library.lib.web.RentalDTO;
import com.library.lib.web.RentalResponse;
import org.springframework.stereotype.Component;

@Component
public class RentalMapper {

    private final UsersRepository userRepository;
    private final BookRepository bookRepository;

    public RentalMapper(UsersRepository userRepository, BookRepository bookRepository) {
        this.userRepository = userRepository;
        this.bookRepository = bookRepository;
    }
    public RentalResponse map(Rental rentalEntity) {
        RentalResponse rentalResponse = new RentalResponse(rentalEntity.getId(), rentalEntity.getRented_at());
        rentalResponse.setBookId(rentalEntity.getBook().getId());
        if(rentalEntity.getUser() != null)
            rentalResponse.setUserId(rentalEntity.getUser().getId());
        return rentalResponse;
    }

    public Rental createNew(RentalDTO rentalDto) {
        Rental rental = new Rental();
        rental.setRented_at(rentalDto.getRented_at());
        rental.setUser(userRepository.findById(rentalDto.getUserId()));
        rental.setBook(bookRepository.findById(rentalDto.getBookId()));
        return rental;
    }
}
