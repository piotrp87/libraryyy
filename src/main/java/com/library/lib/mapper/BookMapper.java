package com.library.lib.mapper;

import com.library.lib.model.Book;
import com.library.lib.repo.AuthorRepository;
import com.library.lib.service.AuthorService;
import com.library.lib.service.RentalService;
import com.library.lib.web.BookDTO;
import com.library.lib.web.BookResponse;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {
    private final AuthorRepository authorRepository;
    private final AuthorService authorService;
    private final RentalService rentalService;


    public BookMapper(AuthorRepository authorRepository, AuthorService authorService, RentalService rentalService) {
        this.authorRepository = authorRepository;
        this.authorService = authorService;
        this.rentalService = rentalService;
    }

    public Book createNew(BookDTO bookDto) {
        Book book = new Book();
        book.setBorrowed(bookDto.isBorrowed());
        book.setTitle(bookDto.getTitle());
        book.setAuthors(authorService.findAuthor(bookDto.getAuthorsId()));
//        book.setRental(rentalService.findRental(bookDto.getRentalId()));
        return book;
    }

    public BookResponse map(Book book) {

//        BookResponse bookResponse = new BookResponse(book.getId(), book.getTitle(),book.getDetails(), book.isBorrowed(), book.isDamaged());
        BookResponse bookResponse = new BookResponse(book.getId(), book.getTitle(),book.getDetails(), book.isBorrowed(), book.isDamaged(),book.getAuthors().getId());

        if(book.isDamaged() !=true)
        bookResponse.setAuthorsId(book.getAuthors().getId());
        if(book.getRental() != null)
            bookResponse.setRentalId(book.getRental().getId());

        return bookResponse;
    }


    public BookResponse map_2(Book book) {
        BookResponse bookResponse = new BookResponse(book.getId(), book.getTitle(),book.getDetails(), book.isBorrowed(), book.isDamaged(),book.getAuthors().getId());
        return bookResponse;
    }

}
