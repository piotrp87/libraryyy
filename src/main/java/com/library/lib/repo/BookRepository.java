package com.library.lib.repo;

import com.library.lib.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;



import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
   // List<Book> findAllBook();
//    List<Book> findByTitle(String title);


    Book findByTitle(String title);


    Book findById(Long id);
//    List<Book> findAll();
}
