package com.library.lib.repo;


import com.library.lib.model.Rental;
import org.springframework.data.jpa.repository.JpaRepository;



import java.util.List;

public interface RentalRepository extends JpaRepository<Rental, Long> {
    Rental findById(Long id);
}
