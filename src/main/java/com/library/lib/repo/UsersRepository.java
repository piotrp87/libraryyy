package com.library.lib.repo;

import com.library.lib.model.User;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findByLastName(String lastName);
    User findById(Long id);
}